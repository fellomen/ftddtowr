#!/bin/bash
DEBUG=false
keep=false
dry_run=false
gif_mode=false
webm_mode=false
files=()

# print usage when no arguments are given
if [ $# -eq 0 ] ; then
    echo "ugoira-to-webm - convert ugoira zip archives to webm/gif [version 1.0.0]"
    echo ""
    echo "Usage:   ugoira-to-webm [options] [files...]"
    echo ""
    echo "A script to convert zip archives of Pixiv's ugoira animations to WebM or GIF."
    echo "The script accepts multiple files at once and will convert each sequentially."
    echo ""
    echo "Personal testing has been done with archives downloaded with:"
    echo "  https://addons.mozilla.org/en-US/firefox/addon/px-downloader"
    echo ""
    echo "Options:"
    echo "   --webm ...... Converts archive to webm format"
    echo "   --gif ....... Converts archive to gif format"
    echo "   --debug ..... Prints additional information"
    echo "   --dry-run ... Prints the resulting ffmpeg command, instead of running it"
    echo "   --keep ...... Keeps the extracted files"
    echo ""
    echo "--webm and --gif can be provided at the same to enable both modes."
    echo "The output name is taken from the input zip file. If it ends with .zip,"
    echo "the file extension is stripped, otherwise it is kept."
    echo "This tool requires jq (JSON parsing) and ffmpeg (video conversion)."
    exit 0
fi

# parse arguments
while [[ $# -gt 0 ]]; do
    case $1 in
        --debug)
            DEBUG=true
            shift
            ;;
        --dry-run)
            dry_run=true
            shift
            ;;
        --keep)
            keep=true
            shift
            ;;
        --webm)
            webm_mode=true
            shift
            ;;
        --gif)
            gif_mode=true
            shift
            ;;
        --*|-*)
            # unknown flag, ignore
            shift
            ;;
        *)
            # treat unknown parameters as input file
            files+=("$1")
            shift
            ;;
    esac
done

print_debug() {
    if [ "$DEBUG" =  true ] ; then
    echo "$1"
    fi;
}

echo_error() {
    echo "$1" 1>&2
}

perform_conversion() {
    tmp_folder="/tmp/$(uuidgen)"
    zip_file="$1"

    # Check if file is actually a zip archive
    file "$zip_file" | grep "zip" >& /dev/null
    is_zip=$?

    if [ $is_zip -ne 0  ] ; then
        echo_error "Input file is not a zip archive. Exiting."
        exit 1
    fi

    # Unzip file
    print_debug "Extracting to folder: $tmp_folder"
    unzip "$zip_file" -d "$tmp_folder" >& /dev/null
    if [ $? -ne 0 ] ; then
        echo_error "Failed extracting zip file. Exiting."
        exit 2
    else
        print_debug "Extracted to: $tmp_folder"
    fi

    # Check for and parse json file
    find . -name "animation.json" >& /dev/null
    if [ $? -ne 0 ] ; then
        echo_error "Did not find animation.json file. Exiting."
        exit 3
    fi
    command -v jq >& /dev/null
    if [ $? -ne 0 ] ; then
        echo_error "This script requires jq to be installed. Exiting."
        echo_error "See: https://github.com/stedolan/jq"
        exit 4
    fi

    ffmpeg_file="$tmp_folder/ffmpeg_concat.txt"
    # Due to a quirk, the last image has to be specified twice - the 2nd time without any duration directive
    last_success_frame_file=""

    while read -r frame_file ; do
        read -r frame_delay
        delay_millis=$(echo "scale=3;$frame_delay/1000" | bc -l | awk '{printf "%.3f\n", $0}')
        echo "file '$tmp_folder/$frame_file'" >> "$ffmpeg_file"
        echo "duration $delay_millis" >> "$ffmpeg_file"
        last_success_frame_file="file '$tmp_folder/$frame_file'"
    done < <(jq ".ugokuIllustData.frames[] | .file, .delay" -r "$tmp_folder/animation.json")
    echo "$last_success_frame_file" >> "$ffmpeg_file"

    # Encode with ffmpeg / Dry-runs
    # Parse out file name
    # Remove .zip from file name, otherwise just use the full name
    out_file=$(basename "$zip_file" ".zip")
    if [ "$gif_mode" = true ] ; then
        if [ "$dry_run" = false ] ; then
            ffmpeg -hide_banner -f concat -safe 0 -i "$ffmpeg_file" "$out_file.gif"
        else
            echo "Dry run active: ffmpeg -hide_banner -f concat -safe 0 -i \"$ffmpeg_file\" \"$out_file.gif\""
        fi
    fi
    if [ "$webm_mode" = true ] ; then
        if [ "$dry_run" = false ] ; then
            ffmpeg -hide_banner -f concat -safe 0 -i "$ffmpeg_file" "$out_file.webm"
        else
            echo "Dry run active: ffmpeg -hide_banner -f concat -safe 0 -i \"$ffmpeg_file\" \"$out_file.webm\""
        fi
    fi

    # Cleanup
    if [ "$keep" = false ] ; then
        print_debug "Removing folder and files: $tmp_folder"
        rm -rf "$tmp_folder"
    else
        print_debug "Keeping folder and files: $tmp_folder"
    fi
}

for file in "${files[@]}"; do
    perform_conversion "$file"
done