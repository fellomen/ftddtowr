#!/bin/bash

screenshot_folder="$HOME/Pictures/Screenshots"
mkdir -p "$screenshot_folder"
file="$screenshot_folder/$(date +%s).png"
maim --hidecursor --select "$file"
maim_result="$?"

if [ $maim_result -ne 0 ]; then
	notify-send "Screenshot failed or cancelled" "Location: $file"
	exit 1
else
	xclip -selection clipboard -t image/png -i "$file"
	notify_result=$(notify-send --icon "$file" -t 5000 -A open="Open in directory" -A show="Show image" "Screenshot saved" "$file")
	if [[ $notify_result = "open" ]]; then
		thunar "$file"
	elif [[ $notify_result = "show" ]]; then
		xdg-open "$file"
	fi
fi
exit 0
