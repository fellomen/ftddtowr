# heap
Files that don't deserve their own repository.

- [screenshot-select-area.sh](./screenshot-select-area.sh)
  - Uses `maim` to take a screenshot of a rectangular area and saves it to `$HOME/Pictures/Screenshots`.
  - Copies image to clipboard.
  - Shows a notification to reveal in thunar (have to change that if you use some other file explorer) or open in default image viewer (using xdg-open).
    - If the screenshot failed/is canceled it shows a failure notification.
- [ugoira-to-webm.sh](./ugoira-to-webm.sh)
  - Tool to convert Pixiv's ugoira zip archives into webms/gifs.